# User Types

Enum с типами пользователей для подключения в любые сервисы ensi

## Установка

`composer require ensi/user-types`

## Использование

Let's create a factory and extend abstract Factory.
All you need is to define `definition` and `make` methods.

```php
use Ensi\UserTypes\UserType;

echo UserType::ADMIN; // admin
echo UserType::CUSTOMER; // customer
echo UserType::SELLER; // seller

var_dump(UserType::cases()); // ['admin', 'customer', 'seller']
```

### Тестирование

1. composer install
2. npm i
3. composer test

## Security Vulnerabilities

Please review [our security policy](../../security/policy) on how to report security vulnerabilities.

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
